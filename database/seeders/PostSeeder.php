<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        for ($i=1; $i <= 2 ; $i++) { 
        	# code...
        	if ($i==1) {
        		# code...
        		DB::table('posts')->insert([
        			'title' => 'Welcome To Portal Berita',
        			'news_content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        			'author' => 1,
        			'created_at'=> date("Y-m-d H:i:s"),


        		]);
        	}else{
        			DB::table('posts')->insert([
        			'title' => 'Pengumuman',
        			'news_content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        			'author' => 1,
        			'created_at'=> date("Y-m-d H:i:s"),

        		]);
        	}
        }
          
    }
}
